
import React, { useEffect, useState, useRef, useCallback } from 'react';
import * as pdfjsLib from "pdfjs-dist/build/pdf";
import pdfjsWorker from "pdfjs-dist/build/pdf.worker.entry";

export default function PDFViewer({ url }) {
   const canvasRef = useRef();
   pdfjsLib.GlobalWorkerOptions.workerSrc = pdfjsWorker;


   const [pdfRef, setPdfRef] = useState();
   const [currentPage, setCurrentPage] = useState(1);

   const renderPage = useCallback((pageNum, pdf = pdfRef) => {
      pdf && pdf.getPage(pageNum).then(function (page) {
         const viewport = page.getViewport({ scale: 1.3 });
         const canvas = canvasRef.current;
         canvas.height = viewport.height;
         canvas.width = viewport.width;
         const renderContext = {
            canvasContext: canvas.getContext('2d'),
            viewport: viewport
         };
         //Step 1: store a refer to the renderer
         const pageRendering = page.render(renderContext);
         //Step : hook into the pdf render complete event
         var completeCallback = pageRendering._internalRenderTask.callback;
         pageRendering._internalRenderTask.callback = function (error) {
            //Step 2: what you want to do before calling the complete method                  
            completeCallback.call(this, error);
            //Step 3: do some more stuff
         };
      });


   }, [pdfRef]);

   useEffect(() => {
      renderPage(currentPage, pdfRef);
   }, [pdfRef]);

   useEffect(() => {
      const loadingTask = pdfjsLib.getDocument(url);
      loadingTask.promise.then(loadedPdf => {
         setPdfRef(loadedPdf);
      }, function (reason) {
         console.error(reason);
      });
   }, [url]);


   const nextPage = () => pdfRef && currentPage < pdfRef.numPages && setCurrentPage(currentPage + 1);

   const prevPage = () => currentPage > 1 && setCurrentPage(currentPage - 1);

   const getCoords = (e) => {
      console.group()
      console.log('width', e.target.width, 'height', e.target.height)
      console.log('x:', e.pageX, 'y:', e.pageY)
      drawRectangle(e.pageX, e.pageY, 150, 90)
      console.groupEnd()
   }


   function drawRectangle(x, y, w, h) {
      const canvas = canvasRef.current;
      const renderContext = canvas.getContext('2d')
      renderContext.strokeStyle = 'blue';
      renderContext.strokeRect(x, y, w, h);
   }

   return (
      <canvas ref={canvasRef} onMouseDown={(e) => getCoords(e)}></canvas>
   );
}