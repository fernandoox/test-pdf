import WebViewer from '@pdftron/webviewer'

export default class PDFTron {
   init = (source, element) => {
      new WebViewer({
         path: '/WebViewer/lib',
         initialDoc: source,
         mouseLeftUp: function (e) {
            const windowCoords = this.getMouseLocation(e);
            console.log(windowCoords)
         }
      }, element).then(instance => {
         instance.disableElements(['toolbarGroup-Edit']);
         instance.disableElements(['toolbarGroup-Insert']);
         const { docViewer, annotManager, Annotations } = instance;
         annotManager.on('annotationChanged', (e) => {
            console.log(e)
         });
         docViewer.on('dblClick', e => { });
      });
   }
}